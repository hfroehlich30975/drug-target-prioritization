library(org.Hs.eg.db) # DB for human genes
library(plyr) # Tools for Splitting, Applying and Combining Data

source(file.path(home, "DrugTargetPrioritization", "ScoringByGOSim.R"))
source(file.path(home, "DrugTargetPrioritization", "ScoringBySequenceSim.R"))

add.features <- function(fld, genes, drug.targets, evalues, no.of.cores=2, no.of.features=6, write.to.file=F) {
  # Apply similarity algorithms
  if (length(fld) < (no.of.features + 2) | RECALCULATE.FEATURES) {

    features <- unique(fld, by="GeneID")

    features <- join.GO.features(features, genes, drug.targets, "BP", no.of.cores)
    features <- join.GO.features(features, genes, drug.targets, "CC", no.of.cores)
    # features <- join.GO.features(features, genes, drug.targets, "MF", no.of.cores)
    # features <- join.seq.features(features, genes, drug.targets, evalues, no.of.cores)

    # Reorder columns
    features <- features[, c("GeneID",
                             "Interconnectivity",
                             "Neighborhood",
                             "NetworkProp",
                             "RandomWalk",
                             "Diffusion",
                             "BP",
                             "CC",
                             # "MF",
                             "Betweenness",
                             "IsTarget")]
    rownames(features) <- features$GeneID
    return(features[ , 2:(no.of.features + 2)])
  }
  fld <- as.data.frame(fld)
  fld <- na.omit(fld)
  rownames(fld) <- fld$GeneID
  return(fld[ , 2:(no.of.features + 2)])
}


join.GO.features <- function(features, genes, drug.targets, ont, no.of.cores) {
  # calculates and then joins the similarity-based features based on ontology
  similarity.scores <- get.GO.sim.scores(drug.targets, genes, ont, no.of.cores)
  df.scores <- data.frame(similarity.scores, as.numeric(names(similarity.scores)))
  colnames(df.scores) <- c(ont, "GeneID")
  df.scores <- unique(df.scores, by="GeneID")
  features <- plyr::join(features, df.scores, by="GeneID")
  return(features)
}

join.seq.features <- function(features, genes, drug.targets, evalues, no.of.cores) {
  similarity.scores <- get.seq.sim.scores(drug.targets, genes, evalues, no.of.cores)
  df.scores <- data.frame(similarity.scores, as.numeric(names(similarity.scores)))
  colnames(df.scores) <- c("SeqSim", "GeneID")
  df.scores <- unique(df.scores, by="GeneID")
  features <- plyr::join(features, df.scores, by="GeneID")
  return(features)
}
