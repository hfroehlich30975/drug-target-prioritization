#!/usr/bin/env python3

"""Command line interface for gene_prioritization.

This can be run as a module with ``python -m gene_prioritization.cli``
or through the installed command ``gene_prioritization.
"""

import logging
import os
import time

import click
from gene_prioritization.config import Params
from gene_prioritization.inputoperations import fetchers, parsers
from gene_prioritization.model.network import Network
from gene_prioritization.networkoperations import network_feature_extractor as nfe
import rpy2.robjects as ro
from tqdm import tqdm

HERE = os.path.abspath(os.path.dirname(__file__))


def set_up(config_path: str) -> Params:
    """Set up the program.

    Check the paths exist, create necessary folders and the Params object
    and download the necessary files.

    :param str config_path: The path to the configuration file.
    :return Params: A Params object containing information on paths, constant
    fields and tool options.
    """
    if not os.path.exists(config_path):
        raise Exception("Could not find the configuration file on path {}".
                        format(config_path))

    params = Params(config_path)
    if not os.path.exists(params.HIPPIE_PATH):
        raise Exception("Could not find the protein-protein interaction "
                        "network file on path {}".format(params.HIPPIE_PATH))
    if not os.path.exists(params.DATA_PATH):
        raise Exception("Could not find the gene expression data file on "
                        "path {}".format(params.DATA_PATH))
    if not os.path.exists(params.DRUG_TARGETS_PATH):
        raise Exception("Could not find the special genes file on "
                        "path {}".format(params.DRUG_TARGETS_PATH))
    if not os.path.isdir(params.OUTPUT_DIR):
        os.makedirs(params.OUTPUT_DIR)

    fetchers.download_tsv_from_gz(params.POTENTIAL_TARGET_URL,
                                  params.POTENTIAL_TARGET_PATH)

    fetchers.download_tsv_from_gz(params.TISSUE_SPEC_TARGET_URL,
                                  params.TISSUE_SPEC_TARGET_PATH)

    check_r_installation()

    return params


def check_r_installation():
    """Check if R and necessary R packages are installed."""
    try:
        home_dir = os.path.join(HERE, "..", "GENHelper", "R", "Utils", "RLoader.R")
        ro.r("source('{}')".format(home_dir))
    except Exception:
        raise Exception("R or necessary R packages could not be loaded. "
                        "Please check your installation")


def generate_ppi_network(params: Params) -> Network:
    """Generate the protein-protein interaction network.

    :param params: A Params object containing information on paths, constant fields and tool options.
    :return Network: Protein-protein interaction network with information on differential expression.
    """
    # Compilation of a protein-protein interaction (PPI) graph (HIPPIE)
    protein_interactions = parsers.parse_hippie(params.HIPPIE_PATH,
                                                params.CONFIDENCE_CUTOFF)
    protein_interactions = protein_interactions.simplify()

    if params.DATA_PATH.endswith('.xlsx'):
        gene_list = parsers.parse_excel(params.DATA_PATH, params)
    elif params.DATA_PATH.endswith('.csv'):
        gene_list = parsers.parse_csv(params.DATA_PATH, params)
    else:
        raise ValueError('Unsupported extension: {}'.format(params.DATA_PATH))

    current_disease_ids = parsers.parse_disease_ids(params.CURRENT_DISEASE_IDS_PATH)
    disease_associations = parsers.parse_disease_associations(params.DISEASE_ASSOCIATIONS_PATH,
                                                              current_disease_ids)

    # Build an undirected weighted graph with the remaining interactions based
    # on Entrez gene IDs
    network = Network(protein_interactions, params=params)
    network.set_up_network(gene_list, disease_associations=disease_associations)

    return network


def write_gat2vec_input_files(network, params):
    """Write the input files for gat2vec tool.

    :param Network network: Network object with attributes overlayed on it.
    :param Params params: Params object that encapsulates paths and other input information.
    """
    adj_list = network.graph.get_adjlist()
    with open(params.GRAPH_ADJLIST_PATH, mode="w") as output_file:
        for i in tqdm(range(len(adj_list))):
            line = str(i) + " " + " ".join(str(s) for s in adj_list[i]) + "\n"
            output_file.write(line)
    network.write_attribute_adj_list(params.ATTRIBUTE_ADJLIST_PATH)
    drug_targets = parsers.parse_gene_list(params.DRUG_TARGETS_PATH, network.graph)
    network.write_index_labels(drug_targets, params.MAPPED_LABELS_PATH)


def rank_genes(network: Network, params: Params, config_path: str) -> None:
    """Rank genes based on a given set of special genes.

    :param Network network: An instance of Network class.
    :param Params params: A Params object containing info on paths, constant fields, and options.
    :param str config_path: The path to the configuration file.
    """
    start = time.time()

    if (not os.path.exists(params.FEATURE_PATH)) or params.RECALCULATE_FEATURES:
        feature_extractor = nfe.NetworkFeatureExtractor(network, params)
        feature_extractor.extract_features()

    r_home = os.path.join(HERE, "..", "GENHelper", "R")
    ro.r("home <- '" + r_home + "'")
    ro.r("config.file <- '" + config_path + "'")

    ro.r("source('{}')".format(
        os.path.join(r_home, "DrugTargetPrioritization.R")))

    print("Drug Target Prioritization - Elapsed time in minutes: {}".format(
        (time.time() - start) / 60))


@click.command()
@click.option('--config-path',
              prompt='Please enter the path to the config file',
              help='Path to config file.')
def main(config_path: str) -> None:
    """Run Gene Prioritization.

    :param str config_path: The path to the configuration file.
    """
    logging.basicConfig(level=logging.INFO)

    start = time.time()

    params = set_up(config_path)
    network = generate_ppi_network(params)

    write_gat2vec_input_files(network, params)

    rank_genes(network, params, config_path)

    # TODO: write visualization pipeline
    # if params.VISUALIZE:

    click.echo("Elapsed time in min: {}".format((time.time() - start) / 60))


if __name__ == '__main__':
    main()
