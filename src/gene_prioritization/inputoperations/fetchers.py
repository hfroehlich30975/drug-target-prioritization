# -*- coding: utf-8 -*-

"""Methods to fetch data online."""

import gzip
import io
import os
from urllib.request import urlopen


def download_tsv_from_gz(url: str, output_path: str) -> None:
    """Download the file from url, unzip it and save it under output_path.

    :param str url: URL of the file to be downloaded
    :param str output_path: Path to save the unzipped tsv file
    """
    response = urlopen(url)

    compressed_file = io.BytesIO()
    compressed_file.write(response.read())
    compressed_file.seek(0)

    decompressed_file = gzip.GzipFile(fileobj=compressed_file, mode='rb')
    decompressed_file.seek(0)

    with open(os.path.expanduser(output_path), 'wb') as out_file:
        out_file.write(decompressed_file.read())
