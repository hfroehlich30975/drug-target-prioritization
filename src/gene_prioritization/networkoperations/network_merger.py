# -*- coding: utf-8 -*-

"""Methods to merge graphs."""

from igraph import Graph


def merge_graphs(graph1: Graph, graph2: Graph, subgraph_name: str) -> Graph:
    """Merge two graphs together, keeping the information on attributes.

    :param Graph graph1: First graph.
    :param Graph graph2: Second graph.
    :param str subgraph_name: The name of subgraph.
    :return Graph: Merged graph.
    """
    # TODO: check if there is a simpler way, perhaps without the loop
    for v in graph2.vs:
        att = v.attributes()
        try:
            graph1.vs.find(name=att['name']).attributes()
        except ValueError:
            graph1.add_vertex(name=att['name'])
            vertex = graph1.vs(name=att['name'])
            vertex['log2_fold_change'] = att['log2_fold_change']
            vertex['symbol'] = att['symbol']
            vertex['padj'] = att['padj']
            vertex['is_diff_expressed'] = att['is_diff_expressed']
            vertex['up_regulated'] = att['up_regulated']
            vertex['down_regulated'] = att['down_regulated']
            vertex['subgraph_name'] = subgraph_name
    for e in graph2.es:
        att = e.attributes()
        # TODO: giving the index should be enough
        graph1.add_edge(graph2.vs[e.source]["name"],
                        graph2.vs[e.target]["name"],
                        weight=att['weight'])
    return graph1
