# -*- coding: utf-8 -*-

"""Methods to visualize protein-protein interaction networks."""

from gene_prioritization.model.network import Network
from gene_prioritization.networkoperations import network_merger as merger
from igraph import _igraph, Graph
import pandas as pd
from py2cytoscape.data.cyrest_client import CyRestClient as Client
from py2cytoscape.data.util_network import NetworkUtil


# TODO: this could be a class

def visualize_neighborhoods(network: Network,
                            nodes: list,
                            transc_factors: list,
                            name: str) -> None:
    """Visualize the neighborhoods of a given list of nodes, and their merge.

    :param Network network: The graph in which the nodes reside.
    :param list nodes: A list of nodes whose neighborhoods are to be plotted.
    :param str name: The prefix for the name of neighborhood graphs.
    """
    # print("In visualize_neighborhoods()")
    merged_network = Graph()

    for node_name in nodes:
        current_network = network.get_neighbor_network(node_name)
        graph_name = name + "-" + node_name

        # py2cytoscape cannot work with too large networks
        if len(current_network.vs) < 3000:
            visualize_network(current_network,
                              graph_name,
                              genes=[node_name],
                              transc_factors=transc_factors)

        merged_network = merger.merge_graphs(merged_network,
                                             current_network,
                                             graph_name)
    if len(merged_network.vs) > 0:
        visualize_network(merged_network, name + "-MergedNetwork",
                          genes=nodes,
                          transc_factors=transc_factors)


def visualize_network(graph: Graph,
                      name: str,
                      genes: list = None,
                      genes2: list = None,
                      transc_factors: list = None) -> None:
    """Visualize graphs, highlighting the special genes and paths between them.

    :param Graph graph: Graph to be visualized.
    :param str name: The name of the graph.
    :param list genes: A list of genes to highlight with color.
    :param list genes2: A list of genes to highlight with color.
    :param list transc_factors: A list of transcription factors to highlight with color.
    """
    # print("In visualize_network()")
    # convert entrez ids to symbols in all lists.
    symbols, symbols2, symbols_tf = [], [], []
    genes2 = list(set(genes2) - set(genes))
    if genes is not None:
        symbols = graph.vs.select(name_in=genes)["symbol"]
    if genes2 is not None:
        symbols2 = graph.vs.select(name_in=genes2)["symbol"]
    if transc_factors is not None:
        symbols_tf = graph.vs.select(name_in=transc_factors)["symbol"]

    # set network name as symbols and add a field for Entrez ids
    graph = _get_network_to_visualize(graph)

    # Start connection
    cy = Client()
    cy_network = cy.network.create_from_igraph(graph, name=name)

    # set up the style
    style = cy.style.create('Minimal')
    cy.style.apply(style=style, network=cy_network)

    # get the view - to use for highlighting
    view_id_list = cy_network.get_views()
    view = cy_network.get_view(view_id_list[0], format='view')

    # Differentiate nodes using shape and color
    diff_expr = graph.vs.select(is_diff_expressed_eq=True)['symbol']
    _change_node_shape(diff_expr, view, cy_network, shape="Triangle")
    # diff_expr = list(((set(diff_expr) - set(symbols)) - set(symbols_tf)) -
    # set(symbols2))
    _change_node_color(diff_expr,
                       view,
                       cy_network,
                       bg_color="#c4c5c6",
                       text_color="black")

    _change_node_color(symbols_tf,
                       view,
                       cy_network,
                       bg_color="yellow",
                       text_color="black")
    _change_node_color(symbols,
                       view,
                       cy_network,
                       bg_color="blue",
                       text_color="white")
    _change_node_color(symbols2,
                       view,
                       cy_network,
                       bg_color="#53e262",
                       text_color="black")

    # Highlight edges between transcription factors and other important genes
    _highlight_edges(diff_expr, symbols_tf, graph, view, cy_network)
    _highlight_edges(symbols,
                     symbols_tf,
                     graph,
                     view,
                     cy_network,
                     color="blue")
    _highlight_edges(symbols2,
                     symbols_tf,
                     graph,
                     view,
                     cy_network,
                     color="#53e262")

    # set and apply the layout
    if len(cy_network.get_nodes()) > 1000:
        cy.layout.apply(network=cy_network, name="circular")
    else:
        cy.layout.apply(network=cy_network, name="kamada-kawai")
    cy.layout.fit(network=cy_network)

    # TODO: save as separate graph files.


def visualize_network_simple(graph: Graph, name: str) -> None:
    """Visualize a given graph.

    :param graph: Graph to visualize.
    :param name: Name of the graph.
    """
    # Connect to Cytoscape
    cy = Client()
    cy_network = cy.network.create_from_igraph(graph, name=name)

    # Set up the style
    style = cy.style.create('Minimal')
    cy.style.apply(style=style, network=cy_network)

    # Set and fit the layout
    cy.layout.apply(network=cy_network, name="circular")
    cy.layout.fit(network=cy_network)


def _change_node_color(node_symbols,
                       view,
                       cy_network,
                       bg_color: str = "#666666",
                       text_color: str = "#FFFFFF") -> None:
    """Change the color of list of nodes.

    :param list node_symbols: Gene symbols of nodes.
    :param client().network.view view: Cytoscape view object.
    :param client().network cy_network: Cytoscape graph object.
    :param str bg_color: Desired node background color.
    :param str text_color: Desired node text color.
    """
    print("In highlight_nodes()")
    node_suids = _get_cy_ids(node_symbols, cy_network)

    # set up the color and label color properties for every node
    dataframe = pd.DataFrame(index=node_suids,
                      columns=['NODE_FILL_COLOR', 'NODE_LABEL_COLOR'])
    for _, row in dataframe.iterrows():
        row['NODE_FILL_COLOR'] = bg_color
        row['NODE_LABEL_COLOR'] = text_color

    # update the node colors in batch
    view.batch_update_node_views(dataframe)


def _change_node_shape(node_symbols,
                       view,
                       cy_network,
                       shape: str = "Triangle",
                       size: int = 100) -> None:
    """Change the shape and size of list of nodes.

    :param list node_symbols: Gene symbols of nodes.
    :param client().network.view view: Cytoscape view object.
    :param client().network cy_network: Cytoscape graph object.
    :param str shape: Desired shape of the node.
    :param int size: Desired size of the node.
    """
    print("In highlight_nodes()")
    node_suids = _get_cy_ids(node_symbols, cy_network)

    # set up the shape and size properties for every node
    df = pd.DataFrame(index=node_suids, columns=['NODE_SHAPE', 'NODE_SIZE'])
    for index, row in df.iterrows():
        row['NODE_SHAPE'] = shape
        row['NODE_SIZE'] = size

    # update the node shapes and size in batch
    view.batch_update_node_views(df)


def _highlight_edges(genes: list,
                     genes2: list,
                     graph: Graph,
                     view,
                     cy_network,
                     color: str = "#d8d33a") -> None:
    """Highlight the edges between the given lists.

    :param list genes: List 1.
    :param list genes2: List 2.
    :param Graph graph: The graph that contains the two lists of genes.
    :param client().network.view view: Cytoscape view object.
    :param client().network cy_network: Cytoscape network object.
    :param color: Edge color.
    """
    # TODO: too complex.
    # For every pair of genes, find the shortest path between them and save the edge ids
    edge_ids = set()
    for gene in genes:
        for factor in genes2:
            try:
                gene_id = graph.vs.find(name=gene).index
                factor_id = graph.vs.find(name=factor).index
                paths = graph.get_shortest_paths(v=gene_id, to=factor_id,
                                                 weights='weight')
                for path in paths:
                    for i in range(len(path) - 1):
                        eid = graph.get_eid(path[i], path[i + 1])
                        edge_ids.add(eid)
            except ValueError:
                pass
            except IndexError:
                pass
            except _igraph.InternalError:
                pass

    # convert edge ids to suitable format
    name_map = NetworkUtil.name2suid(cy_network, obj_type='edge')
    edge_suids = []
    for id in edge_ids:
        edge_suids.append(name_map[id])

    # set edge color property for every edge
    df = pd.DataFrame(index=edge_suids,
                      columns=['EDGE_STROKE_UNSELECTED_PAINT'])
    for index, row in df.iterrows():
        row['EDGE_STROKE_UNSELECTED_PAINT'] = color

    # update the edge colors in batch
    view.batch_update_edge_views(df)


def _get_cy_ids(names: list, cy_network) -> list:
    """Return the cytoscape ids of nodes.

    :param list names: symbols of nodes.
    :param client().network cy_network: Cytoscape graph object.
    :return list: Cytoscape ids.
    """
    print("In get_cy_ids()")
    name_map = NetworkUtil.name2suid(cy_network)
    ids = []
    for name in names:
        try:
            ids.append(name_map[str(name)])
        except KeyError:
            pass
    return ids


def _get_network_to_visualize(graph: Graph) -> Graph:
    """Set the node names to symbols after saving the names to entrez_id field.

    :param graph: Graph object.
    :return: New network with symbols as names of nodes.
    """
    graph = graph.copy()
    symbols = graph.vs['symbol']
    entrez_ids = graph.vs['name']
    graph.vs['name'] = symbols
    graph.vs['entrez_id'] = entrez_ids
    graph.es['name'] = graph.es.indices
    return graph
