"""This module enables gene_prioritization to be run as ``python -m gene_prioritization``."""

from gene_prioritization.cli import main

if __name__ == '__main__':
    main()
