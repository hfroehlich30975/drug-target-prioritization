# -*- coding: utf-8 -*-

"""Unit tests package."""

import unittest

from gene_prioritization.tests.network_feature_extractor_test import NetworkFeatureExtractorTest
from gene_prioritization.tests.network_test import NetworkTest

if __name__ == '__main__':
    unittest.main()
