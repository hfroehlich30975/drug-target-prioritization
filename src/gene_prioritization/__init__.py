"""Tool for analyzing gene expression using a protein protein interaction network."""

__version__ = '0.0.1-dev'

__title__ = 'gene_prioritization'
__description__ = 'A tool for ranking potential drug targets for a given ' \
                  'disease'
__url__ = 'https://github.com/LifeScienceDataAnalytics/drug-target-prioritization'

__author__ = 'Özlem Muslu'
__email__ = 'ozlemmuslu@gmail.com'

__license__ = 'MIT License'
__copyright__ = 'Copyright (c) 2018 Özlem Muslu'
