# -*- coding: utf-8 -*-

"""Configuration for gene-prioritization."""

import configparser
import os


class Params:
    """Encapsulate information on paths, constant fields and program options."""

    def __init__(self, config_path: str):
        """Initialize the parameters.

        :param str config_path: The path to the configuration file.
        """
        config = configparser.ConfigParser()
        config.read(config_path)
        default = config["DEFAULT"]
        options = config["OPTIONS"]
        exp_file = config["EXP_FILE"]
        paths = config["PATHS"]

        # set values for differentiating differentially expressed genes
        self.MIN_L2FC = float(default["minimum_log2_fold_change"])
        self.MAX_L2FC = float(default["maximum_log2_fold_change"])
        self.MAX_PADJ = float(default["maximum_adjusted_p_value"])
        self.CONFIDENCE_CUTOFF = float(default["interaction_confidence_cutoff"])

        # set options on tasks to-do
        self.VISUALIZE = options.getboolean("visualize")
        self.RECALCULATE_FEATURES = options.getboolean(
            "recalculate_drug_target_features"
        )
        self.DIFF_TYPE = options["diff_type"]
        self.DATASET_NAME = options["dataset_name"]

        # set paths
        self.INPUT_DIR = paths["input_directory"]
        self.HIPPIE_PATH = os.path.join(self.INPUT_DIR, paths["protein_protein_interaction_graph"])
        self.DATA_PATH = os.path.join(self.INPUT_DIR, paths["experiment_file"])
        self.DRUG_TARGETS_PATH = os.path.join(self.INPUT_DIR, paths["drug_targets_file"])
        if paths["transcription_factors_file"] is not "":
            self.TRANSC_FACTOR_PATH = os.path.join(self.INPUT_DIR,
                                                   paths["transcription_factors_file"])
        self.OUTPUT_DIR = paths["output_directory"]
        self.DISEASE_ASSOCIATIONS_PATH = os.path.join(self.INPUT_DIR, paths["disease_associations"])
        self.CURRENT_DISEASE_IDS_PATH = os.path.join(self.INPUT_DIR, paths["disease_ids"])

        # set exp_file
        self.BASE_MEAN = exp_file["base_mean_name"]
        self.L2FC = exp_file["log2_fold_change_name"]
        self.PADJ = exp_file["adjusted_p_value_name"]
        self.ENSEMBL = exp_file["ensembl_id_name"]
        self.SYMBOL = exp_file["gene_symbol_name"]
        self.ENTREZ = exp_file["entrez_id_name"]
        self.SHEET_NAME = exp_file["sheet_name"]
        self.SPLIT_CHAR = exp_file["split_character"]
        self.ID_TYPE = exp_file["id_type"]

        feature_file_name = "NetworkAnalysis-{}.tsv".format(self.DIFF_TYPE)
        self.FEATURE_PATH = os.path.join(paths["output_directory"], feature_file_name)
        self.DRUG_CANDIDATES_PATH = os.path.join(paths["output_directory"],
                                                 "predicted_drug_targets.txt")

        self.POTENTIAL_TARGET_URL = "https://www.proteinatlas.org/search/" \
                                    "protein_class%3APotential+drug+targets?" \
                                    "format=tsv"
        self.TISSUE_SPEC_TARGET_URL = "https://www.proteinatlas.org/search/" \
                                      "tissue_specificity_rna%3A" + \
                                      options["tissue_type"].replace(" ", "+") + \
                                      "%3Belevated+AND+sort_by" \
                                      "%3Atissue+specific+score?" \
                                      "format=tsv"
        self.POTENTIAL_TARGET_PATH = os.path.join(self.INPUT_DIR, "Potential_drug_targets.tsv")
        self.TISSUE_SPEC_TARGET_PATH = os.path.join(self.INPUT_DIR,
                                                    "Tissue_specific_drug_targets.tsv")

        graph_adjlist_name = "{}_graph.adjlist".format(self.DATASET_NAME)
        self.GRAPH_ADJLIST_PATH = os.path.join(self.OUTPUT_DIR, graph_adjlist_name)
        attribute_adjlist_name = "{}_na.adjlist".format(self.DATASET_NAME)
        self.ATTRIBUTE_ADJLIST_PATH = os.path.join(self.OUTPUT_DIR, attribute_adjlist_name)
        self.MAPPED_LABELS_PATH = os.path.join(self.OUTPUT_DIR, "labels_maped.txt")
