Gene Prioritization
===================
OVERVIEW
--------
This is a tool for prioritizing genes based on their similarity to a given set of genes (e.g. disease related genes, drug targets...). It uses information on proximity in protein-protein interaction networks, differential gene expression and shared GO terms. It also has an option to visualize the neighborhoods of highly-prioritized genes.

The user can set up the options to run the tool using the configuration file. More details can be found under README_CONFIGURATION.txt file.

HARDWARE REQUIREMENTS
---------------------
The hardware requirements to run the program are:

1. At least 8GB of RAM

2. At least 4 cores in the CPU are recommended

SOFTWARE REQUIREMENTS
---------------------
The software requirements to run the program are:

1. Ubuntu (>=16.04). You can try other OS, but they are not supported

2. Python (>=3.5)

3. R (>=3.3.3)

4. Java (>=1.8)

5. Cytoscape (>=3.2.1) (For visualization. If you do not wish to visualize, set the relevant fields to False, and then you do not need to install Java or Cytoscape)

6. All Python libraries specified under requirements.txt

7. All R packages specified under DESCRIPTION

INPUT FILES
-----------
There are 4 files which are necessary to run this program:

1. A protein-protein interaction network in the format of:

    **EntrezID** **EntrezID** **CONFIDENCE**
    
    
    Such as:
    
    216 216 0.76
    
    3679 1134 0.73
    
    55607 71 0.65
    
    5552 960 0.63
    
    2886 2064 0.9
    
    5058 2064 0.73
    
    1742 2064 0.87
    
    An example of such a network can be found [here](http://cbdm-01.zdv.uni-mainz.de/~mschaefer/hippie/download.php)


2. An experiment file, in Excel format. Rows show individual entries, columns are the values of the following properties:
	- **Base mean**
	- **Log fold change**
	- **Statistic**
	- **Adjusted p value**
	- **Ensembl id**
	- **Gene symbol**
	- **Entrez id**

    The file may contain other columns too, but the indices and names of the above columns must be entered to the configuration file.

3. A list of Entrez ids of known drug targets, in the format of

    EntrezID1
    
    EntrezID2
    
    ...
    
    
    Such as:
    
    1742
    
    3996
    
    150
    
    152
    
    151

4. A list of Entrez ids of transcription factors, in the format of

    EntrezID1
    
    EntrezID2
    
    ...
    
    
    Such as:
    
    1820
    
    10054
    
    170302
    
    7832
    
    1045

5. A configuration file. The path to the file is input at the beginning of the program. Details can be found under README_CONFIGURATION.md

HOW TO RUN
----------
If you chose to visualize, open Cytoscape.

1. Set up the configuration file. Details can be found under README_CONFIGURATION.md
2. Install the software requirements as specified in this document.
3. Run the Python file \_\_main\_\_.py
4. You will be prompted for the path to the configuration file. Enter the absolute path or the path relative to the directory you are in. 
5. The program should start.
