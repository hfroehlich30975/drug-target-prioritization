#!/usr/bin/env python3
# import rpy2's package module

import rpy2.robjects.packages as rpackages
from rpy2.robjects.vectors import StrVector
import rpy2.robjects as ro

# TODO: include libPaths(generate dir for this purpose?)
utils = rpackages.importr('utils')

# select a mirror for R packages
# utils.chooseCRANmirror(ind=1)  # select the first mirror in the list
# R package names
package_names = [
    'caret',
    'AppliedPredictiveModeling',
    'pROC',
    'RobustRankAggreg',
    'plyr',
    'pracma',
    'tcltk',
    'igraph',
    'readxl',
    'ini',
    'BiocInstaller',
    'devtools',
    'e1071',
]

# Selectively install what needs to be install.
names_to_install = [
    package_name
    for package_name in package_names
    if not rpackages.isinstalled(p)
]

if names_to_install:
    utils.install_packages(StrVector(names_to_install),
                           repos='http://cran.rstudio.com/')

if not rpackages.isinstalled("BiocInstaller"):
    utils.install_packages("BiocInstaller",
                           repos="https://bioconductor.org/packages/3.4/bioc")

ro.r("install_github('ggbiplot', 'vqv')")
ro.r("source('https://bioconductor.org/biocLite.R')")
ro.r("biocLite(c('GOSim', 'clusterProfiler', 'org.Hs.eg.db', 'GO.db', "
     "'topGO'), suppressUpdates=T, suppressAutoUpdate=T, ask=F)")
